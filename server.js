const express = require('express');
const app = express();
const api = require('./routes');
// const sql = require('./config/database');

let hostname = 'localhost';
let port = 8888;

require('dotenv').config();

app.use('/api/v1.0', api);

app.listen(port, hostname, () => {
	console.log(`Server is running at http://${hostname}:${port}`);
});