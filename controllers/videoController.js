const {google} = require('googleapis');
const csvtojson=require("csvtojson");
const bodyParser = require('body-parser');
const conn = require('../config/database');
const csv=require('csvtojson');
const csvFilePath='./storage/videodata.csv';
const fs = require('fs');


module.exports = {
	processCSV: processCSV,
	saveVideo: saveVideo,
	getPlaylistItems: getPlaylistItems,
	getVideo: getVideo,
	getPlaylistVideos: getPlaylistVideos,
	getMultiplePlaylistVideos: getMultiplePlaylistVideos
}

/*
	save data from csv to database
*/
async function processCSV() { 
	const jsonArray=await csv().fromFile(csvFilePath);
	var values = [];
	for(var i=0; i< jsonArray.length; i++) {
		values.push([jsonArray[i].video_id,jsonArray[i].channel_id]);
	}

	conn.query("INSERT INTO csv_data (video_id, channel_id) VALUES ?", [values], function(err,result) {
		if(err) {
		    throw err;
		} else {
	    	console.log('insert into csv_data successful');
	  	}
	});
}

/*
	get video_id from database,
	get video info from youtube api,
	call saveVideoTodB to save video data,
	update flag = 1 in csv_data
*/
async function saveVideo() {
	const youtube = google.youtube({
	  version: 'v3',
	  auth: process.env.YOUTUBE_CLIENT_ID
	});
	var video_id_list = [];
	var videoJson = [];

	videos = await getVideoIds().then( async (result) => {
		for (i=0; i <result.length ; i++) {
			var video = await youtube.videos.list({
				id: result[i].video_id,
				part: 'snippet',
			});

			videoJson.push(JSON.stringify(video.data));
			video_id_list.push(result[i].video_id);
		}

		return [video_id_list, videoJson];
	});

	saveVideoTodB(videos[1]).then((result) => {
		conn.query("UPDATE csv_data SET flag = 1  WHERE video_id IN ?", [[video_id_list]], function(err,result) {
			if(err) {
			    throw err;
			} else {
			    console.log('update csv_data flag successful');
			}
		});
	});
}


/*
	get video ids from csv_data
*/
function getVideoIds() {
	return new Promise( function (resolve, reject) {
		conn.query("SELECT video_id FROM csv_data where flag != 1", function (err, result, fields) {
			if (err) throw err;
			resolve(result);
		});
	});
}


/*
	save video  info into database
*/
async function saveVideoTodB(videoJson) {
	
	return new Promise( function (resolve, reject) { 
		for(i=0; i < videoJson.length; i++) {
			var values = [];
			var json = JSON.parse(videoJson[i]);
			
			if(json.items.length != 0) {
				json.items.forEach( async (obj) => {
					if (obj.snippet.hasOwnProperty('tags')) {
						tags = obj.snippet.tags.join(',');
					} else {
						tags = "";
					}
					values.push(
						obj.id, obj.snippet.channelId, obj.snippet.channelTitle, obj.snippet.title, obj.snippet.description, obj.etag, obj.publishedAt, 
						obj.snippet.thumbnails.default.url, obj.snippet.thumbnails.default.width, obj.snippet.thumbnails.default.height,
						tags, obj.snippet.position
					);
					
				});

				conn.query("INSERT INTO videos (video_id, channel_id, channel_title, video_title, description, etag, published_at, default_thumbnail_url, default_thumbnail_width, default_thumbnail_height, tags, position) VALUES ?", [[values]], function(err,result) {
					if(err) {
					    throw err;
					} else {
					    console.log('insert into videos successful');
					}
					resolve(result);
				});
			}
		}		
	});
}


/*
	get channel id,
	get channel playlist from youtube api
*/
async function getPlaylistItems() {
	const youtube = google.youtube({
	  version: 'v3',
	  auth: process.env.YOUTUBE_CLIENT_ID
	});

	await getplaylistIds().then( async (result) => {
		for (i=0; i < result.length; i++) {
			var playlist = await youtube.playlistItems.list({
				playlistId: result[i].playlist_id,
				part: 'snippet',
			})

			await getvideoidsInPlayist(JSON.stringify(playlist.data)).then((values) => {
				conn.query("UPDATE videos SET playlist_id = ? WHERE video_id IN ?", [result[i].playlist_id, [values]], function(err,result) {
					if(err) {
					    throw err;
					}
					else {
					    console.log('update videos set successful');
					}
				});
			});			
		}
	});	
}

/*
	get playlist id
*/
function getplaylistIds() {
	return new Promise( function (resolve, reject) {
		conn.query("SELECT playlist_id FROM playlists_csv", function (err, result, fields) {
			if (err) throw err;
			resolve(result);
		});
	});
}


/*
	get video id in playlist
*/
function getvideoidsInPlayist(playlist) {
	var values = [];
	return new Promise( function (resolve, reject) { 
		var json = JSON.parse(playlist);
		if(json.items.length != 0) {
			json.items.forEach( async (obj) => {
				values.push(obj.snippet.resourceId.videoId);
			});
		}
		resolve(values);
	});
}

/*
	get videos in a playlist
*/
async function getPlaylistVideos(req,res) {
	conn.query(`SELECT * FROM videos where playlist_id=?`, [req.params.id], function (err, result, fields) {
		result = JSON.stringify(result);
		// console.log(result);
		// return result;
		res.send(result);
	});
}

/*
	get videos from a multiple playlist)id query
*/
function getMultiplePlaylistVideos(req,res) {
	conn.query(`SELECT * FROM videos where playlist_id in ?`, [[req.query.pid]], function (err, result, fields) {
		result = JSON.stringify(result);
		// console.log(result);
		// return result;
		res.send(result);
	});
}


/*
	get video
*/
async function getVideo(req,res) {
	conn.query(`SELECT * FROM videos where video_id=?`, [req.params.id], function (err, result, fields) {
		result = JSON.stringify(result);
		// return result;
		res.send(result);
	});
}

