const mysql = require('mysql');

var conn = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "",
  database: "videomanager"
});

conn.connect(function(err) {
  if (err) {
    console.error('mysql error connecting: ' + err.stack);
    return;
  }
 
  console.log('mysql connected as id ' + conn.threadId);
});

module.exports = conn;