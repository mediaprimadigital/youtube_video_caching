const express = require('express');
const router = express.Router();
const videoController = require('./controllers/videoController');

router.get('/processcsv/', videoController.processCSV);
router.get('/savevideo/', videoController.saveVideo);
router.get('/getplaylistitems/', videoController.getPlaylistItems);
router.get('/getplaylistvideos/:id', videoController.getPlaylistVideos);
router.get('/getvideo/:id', videoController.getVideo);
router.get('/getmultipleplaylistvideos', videoController.getMultiplePlaylistVideos);


module.exports = router;